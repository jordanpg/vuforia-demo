using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayInfo
{
    public int index { get; protected set; }
    public float width { get; protected set; }
    public float height { get; protected set; }
    public float x { get; protected set; }
    public float y { get; protected set; }
    public float physicalWidth { get; protected set; }

    public DisplayInfo(JSONObject obj)
    {
        index = (int)obj.GetField("index").n;

        width = obj.GetField("width").n;
        height = obj.GetField("height").n;
        x = obj.GetField("x").n;
        y = obj.GetField("y").n;
        physicalWidth = obj.GetField("physicalWidth").n;
    }
}