﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vuforia;

using ZXing;
using ZXing.QrCode;
using ZXing.Common;

[AddComponentMenu("System/VuforiaScanner")]
public class QRCodeBehavior : MonoBehaviour
{
	// Public fields
	public PIXEL_FORMAT targetPixelFormat = PIXEL_FORMAT.RGB888;
	public bool autoFocus = true;
	public float scanTimeout = 1f;

	// Private fields
	private BarcodeReader qrReader;

	private PIXEL_FORMAT pixelFormat;
	private bool formatRegistered = false; // Keeps track of whether we've enabled an image format for camera input.
	private bool autoFocusOn = false;
	private float lastScan = -1f;

	private string lastData = null;
	private float lastDataTime = -1;

	// Properties
	public string LastData { get {return lastData;} }
	public float LastDataTime { get {return lastDataTime;} }

	// Start is called before the first frame update
	void Start()
    {
		// Use greyscale if we're in the Unity editor, otherwise, the image format is governed by the targetPixelFormat field.
		#if UNITY_EDITOR
		pixelFormat = PIXEL_FORMAT.GRAYSCALE;
		#else
		pixelFormat = targetPixelFormat;
		#endif

		// Set up a callback that Vuforia will call when it's ready
		VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
		// Set up a callback to check the camera right after it updates
		VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
		// Set up a callback so we can register/unregister the pixel format when Vuforia unpauses/pauses
		VuforiaARController.Instance.RegisterOnPauseCallback(OnPause);

		// Set up our QR code reader
		qrReader = new BarcodeReader();
    }

	// Called when Vuforia starts
	private void OnVuforiaStarted()
	{
		RegisterCameraFormat();

		if(autoFocus) // Try to force autofocus if we have it enabled
		{
			autoFocusOn = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
			// Even if we try to force it, some devices just won't support it.
			if (!autoFocusOn) CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		}
	}

	// Called after every Vuforia update
	private void OnTrackablesUpdated()
	{
		if(formatRegistered && (Time.time - lastScan) > scanTimeout)
		{
			try
			{
				Vuforia.Image image = CameraDevice.Instance.GetCameraImage(pixelFormat);
				if (image != null) // Check if the image is null, since it may take a few frames to wake up.
				{
					Result data = qrReader.Decode(image.Pixels, image.BufferWidth, image.BufferHeight, RGBLuminanceSource.BitmapFormat.RGB24);
					lastScan = Time.time;
					if (data == null) // No QR code detected.
						return;

					// Save the data and current time
					lastData = data.Text;
					lastDataTime = Time.time;
					Debug.Log(data.Text);
				}
			}
			catch(System.Exception error)
			{
				Debug.LogError(error.Message);
			}
		}
	}

	// Called when Vuforia is paused/unpaused
	private void OnPause(bool paused)
	{
		if (paused)
			UnregisterCameraFormat();
		else
			RegisterCameraFormat();
	}

	private void RegisterCameraFormat()
	{
		// To get information from the camera, we have to tell Vuforia what format we want the image in.
		if(CameraDevice.Instance.SetFrameFormat(pixelFormat, true))
		{
			Debug.Log("Registered pixel format " + pixelFormat.ToString());

			formatRegistered = true;
		}
		else // Format must not be supported
		{
			Debug.LogError("Could not register pixel format " + pixelFormat.ToString() + " on this device.");

			formatRegistered = false;
		}
	}

	private void UnregisterCameraFormat()
	{
		Debug.Log("Unregistering pixel format " + pixelFormat.ToString());
		CameraDevice.Instance.SetFrameFormat(pixelFormat, false);
		formatRegistered = false;
	}
}
