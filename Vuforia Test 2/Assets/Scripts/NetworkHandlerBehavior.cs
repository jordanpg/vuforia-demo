﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkHandlerBehavior : MonoBehaviour
{
	public GameObject socketPrefab;
	public string urlFormat = "ws://{0}:{1}/socket.io/?EIO=4&transport=websocket";
    public bool autoConnectOnDesktop = true;

	private QRCodeBehavior qr = null;
	private float lastConnectTime = -1;
	private Dictionary<string, GameObject> sockets;

    // Start is called before the first frame update
    void Start()
    {
		qr = Camera.main.GetComponent<QRCodeBehavior>();

		sockets = new Dictionary<string, GameObject>();

		// Turn on local autoconnect if we're not on mobile
		SocketIO.SocketIOComponent sock = GetComponent<SocketIO.SocketIOComponent>();
		if(sock && !Application.isMobilePlatform && autoConnectOnDesktop)
			sock.autoConnect = true;
    }

    // Update is called once per frame
    void Update()
    {
		// Keep checking for the QR code object if we don't have one
        if(qr == null) qr = Camera.main.GetComponent<QRCodeBehavior>();

		if(qr != null)
		{
			string val = qr.LastData;
			// Debug.Log(string.Format("Data: {0}", val));
			if (val == null) return;

			// Debug.Log("Checking QR Value");
			if(val.StartsWith("JSCK")) // Signature for new connection
			{
				//DebugConsole.Log("Attempting to create socket");
				VLNetworkObject netObj = JsonUtility.FromJson<VLNetworkObject>(val.Remove(0, 4));
				//Debug.Log(string.Format("{0}, {1}:{2}", netObj.id, netObj.localIP, netObj.port));

				if (sockets.ContainsKey(netObj.localIP) && sockets[netObj.localIP].name == netObj.id) return; // Don't create more sockets for the same connection

				DebugConsole.Log(string.Format("Creating socket {0}, IP: {1}:{2}", netObj.id, netObj.localIP, netObj.port));
				VLMaster.Instance.AddNetworkObject(netObj);
				GameObject newSock = Instantiate(socketPrefab);
				netObj.socketObj = newSock;

				sockets.Add(netObj.localIP, newSock);
				newSock.name = netObj.id;

				SocketIO.SocketIOComponent sock = newSock.GetComponent<SocketIO.SocketIOComponent>();

				sock.url = string.Format(urlFormat, netObj.localIP, netObj.port);
			}
		}
	}
}
