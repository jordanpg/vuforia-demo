﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vuforia;

/*
    VLMaster is a singleton class that governs high-level interactions
    between different parts of the visual link system.
    In particular, it facilitates cooperation between components for
    networked actions such as display registration and screen object tracking.
*/
public class VLMaster : Singleton<VLMaster>
{
    // If true, this will redirect stdout to Debug
    // This is useful for seeing debug messages from non-Unity libraries.
    // (mostly websocket-sharp in this case)
    public bool redirectStdOut = false;
    public GameObject objectPrefab; // Prefab for screen object trackers

    private List<VLNetworkObject> netObjs; // All network objects
    // private Dictionary<int, GameObject> displays; // All displays
    private Dictionary<string, GameObject> displaysIP;
    private Dictionary<string, GameObject> objects; // All screen objects
    private Dictionary<string, JSONObject> objectsMeta; // Screen objects' JSONObjects as sent by servers
    private Dictionary<string, List<string>> relations; // Object relationships
    private Dictionary<string, List<string>> children; // Parent-child relationships

    // These are used when registering displays to allow the new display's
    // properties to be globally known.
    public VLNetworkObject currDisplayNetObj { get; protected set; }
    public int currDisplayIndex { get; protected set; }
    public DisplayInfo currDisplayInfo { get; protected set; }

    // Currently unused, may be used in the future to auto-generate target objects
    public DataSet targetDataset { get; protected set; }

    private PipeSystem pipesys;

    // Prevent use of this constructor outside
    protected VLMaster()
    {
        this.netObjs = new List<VLNetworkObject>();
        // this.displays = new Dictionary<int, GameObject>();
        this.displaysIP = new Dictionary<string, GameObject>();
        this.objects = new Dictionary<string, GameObject>();
        this.objectsMeta = new Dictionary<string, JSONObject>();
        this.relations = new Dictionary<string, List<string>>();
        this.children = new Dictionary<string, List<string>>();

        currDisplayNetObj = null;
        currDisplayIndex = -1;

        if(redirectStdOut)
            UnitySystemConsoleRedirector.Redirect();
    }

    public static string GetDisplayIdentifier(int id, string ip)
    {
        return id.ToString() + "@" + ip;
    }

    public void Start()
    {
        pipesys = GetComponent<PipeSystem>();
        // VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
    }

    public void OnVuforiaStarted()
    {
        ObjectTracker objTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

        // Create a dataset for instant image targets
        targetDataset = objTracker.CreateDataSet();

        int targetFps = VuforiaRenderer.Instance.GetRecommendedFps(VuforiaRenderer.FpsHint.FAST);
        if (Application.targetFrameRate != targetFps) Application.targetFrameRate = targetFps;
    }

    public void RegisterDisplay(VLNetworkObject netObj, int dispIndex, JSONObject obj)
    {
        currDisplayIndex = dispIndex;
        currDisplayNetObj = netObj;
        currDisplayInfo = new DisplayInfo(obj);

        if(dispIndex >= netObj.numDisplays)
            Debug.LogWarning(string.Format("Network object only reports {0} displays, but attempting to register display #{1}.\nSomething is probably wrong.", netObj.numDisplays, dispIndex + 1));
    }

    public void AddNetworkObject(VLNetworkObject netObj)
    {
        if(!netObjs.Contains(netObj)) netObjs.Add(netObj);
    }

    public void SetDisplay(int dispIndex, string ip, GameObject obj)
    {
        // displays[dispIndex] = obj;
        displaysIP[GetDisplayIdentifier(dispIndex, ip)] = obj;
        currDisplayIndex = -1;
        currDisplayNetObj = null;
    }

    public VLNetworkObject GetNetObjFromGameObj(GameObject gameObj)
    {
        foreach(VLNetworkObject i in netObjs)
        {
            if(i.socketObj == gameObj) return i;
        }

        return null;
    }

    public GameObject AddObjectTracker(JSONObject obj)
    {
        if(objects.ContainsKey(obj["id"].str)) return null;

        string identifier = GetDisplayIdentifier((int)obj["disp"].n, obj["ip"].str);
        
        if(!displaysIP.ContainsKey(identifier)) return null;
        DebugConsole.Log(string.Format("Object {0} on display {1}", obj["id"].str, identifier));

        // Find the display object associated with this object
        GameObject dispObj = displaysIP[identifier];
        // Create tracker as child of display
        GameObject newObj = Instantiate(objectPrefab, dispObj.transform);

        // Add list of children
        children.Add(obj["id"].str, new List<string>());

        objects[obj["id"].str] = newObj;

        // Save object relations
        JSONObject objInfo = obj["obj"];
        List<string> l = relations[obj["id"].str] = new List<string>();
        foreach (var item in objInfo["relations"].list)
            l.Add(item.str);

        // Add object to parent if it exists
        if(children.ContainsKey(obj["parent"].str))
            children[obj["parent"].str].Add(obj["id"].str);

        UpdateObjectTracker(obj);

        return newObj;
    }

    public void UpdateObjectTracker(JSONObject obj)
    {
        string id = obj["id"].str;
        if(!objects.ContainsKey(id)) return;

        Debug.Log(string.Format("Updating {0}", id));
        Debug.Log(obj.ToString());

        // Add updated JSONObject to objectsMeta
        objectsMeta[id] = obj;

        string identifier = GetDisplayIdentifier((int)obj["disp"].n, obj["ip"].str);
        if(!displaysIP.ContainsKey(identifier)) return;

        GameObject tracker = objects[id];
        GameObject dispObj = displaysIP[identifier];

        // Update tracker's parent if they moved to a different display
        if(dispObj != tracker.transform.parent.gameObject)
            tracker.transform.parent = dispObj.transform;

        VLDisplayBehavior dispB = dispObj.GetComponent<VLDisplayBehavior>();
        DisplayInfo dispInfo = dispB.Info;
        Bounds dispBounds = dispB.bounds;
        
        float w = obj["width"].n;
        float h = obj["height"].n;
        // Adjust positions to represent center of object in screen space
        float x = obj["x"].n + w / 2;
        float y = obj["y"].n + h / 2;
        // Add parent's screen position if applicable
        // TODO: find a solution that allows multiple nesting.
        if(objectsMeta.ContainsKey(obj["parent"].str))
        {
            x += objectsMeta[obj["parent"].str]["x"].n;
            y += objectsMeta[obj["parent"].str]["y"].n;
        }
        // Normalize screen coordinates
        float nx = x / dispInfo.width;
        float ny = y / dispInfo.height;
        float nw = w / dispInfo.width;
        float nh = h / dispInfo.height;
        // Calculate AR coordinates (Y must be flipped)
        float px = dispBounds.min.x + dispBounds.size.x * nx;
        float py = dispBounds.max.z - dispBounds.size.z * ny;

        // Move and resize tracker
        tracker.transform.localPosition = new Vector3(px, 0, py);
        tracker.transform.localScale = new Vector3(dispBounds.size.x * nw, dispBounds.size.y * 1.1f, dispBounds.size.z * nh);

        Debug.Log(tracker.transform.localPosition.ToString());
        Debug.Log(tracker.transform.localScale.ToString());

        // Trigger updates for all children of this object
        foreach (string child in children[id])
            if (objectsMeta.ContainsKey(child)) UpdateObjectTracker(objectsMeta[child]);

        EstablishConnections();
    }

    public void RemoveObjectTracker(string id)
    {
        if(!objects.ContainsKey(id)) return;

        Destroy(objects[id]);
        objects.Remove(id);
        objectsMeta.Remove(id);
        children.Remove(id);

        DebugConsole.Log(string.Format("Removing object {0}", id));
    }

    // Create links between screen objects
    protected void EstablishConnections()
    {
        foreach (var item in objects)
        {
            if(!relations.ContainsKey(item.Key)) continue;

            foreach (var o in relations[item.Key])
            {
                if(!objects.ContainsKey(o)) continue;

                pipesys.Connect(item.Value.transform, objects[o].transform);
            }
        }
    }
}
