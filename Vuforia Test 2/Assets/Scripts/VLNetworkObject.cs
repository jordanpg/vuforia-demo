﻿using System;
using UnityEngine;

[Serializable]
public class VLNetworkObject
{
	public string id;
	public int port;
	public string localIP;
	public int numDisplays;
	public GameObject socketObj;
}
