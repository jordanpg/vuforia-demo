using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

/*
    VLDisplayBehavior handles the tracking and registration of displays.
    It contains functionality to register its GameObject as a display and
    update its image target over the associated network connection.
*/
public class VLDisplayBehavior : DefaultTrackableEventHandler
{
    public VLNetworkObject NetworkObject { get; protected set; }
    public int DisplayID { get; protected set; }
    public DisplayInfo Info { get; protected set; }
    public Bounds bounds { get; protected set; }

    public int displayIdDebug = -1;

    public string targetReplaceDebug;
    public bool targetReplaceSwitch = false;

    private DataSet ourDataSet;
    public Texture2D nt;
    public bool debugSwap = false;

    protected override void Start()
    {
        bounds = GetComponent<MeshFilter>().mesh.bounds;

        base.Start();

        Debug.Log(string.Format("{0} {1} {2}", name, GetComponent<MeshFilter>().mesh.bounds.size, GetComponent<MeshFilter>().mesh.bounds.extents));

        OnTargetFound.AddListener(RegisterDisplay);

        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
    }

    protected void Update()
    {
        if(targetReplaceSwitch && ourDataSet != null)
        {
            SwitchTarget(GUIUtility.systemCopyBuffer);
            targetReplaceSwitch = false;
        }
    }

    private void OnGUI()
    {
        if(debugSwap && nt)
        {
            GUI.DrawTexture(new Rect(10, 10, 100, 100 * (nt.height / nt.width)), nt);
        }    
    }

    protected void OnVuforiaStarted()
    {
        ImageTargetBehaviour it = GetComponent<ImageTargetBehaviour>();
        if(it)
        {
            Debug.Log(gameObject.name);
            Debug.Log(it.GetSize());
            //Debug.Log(string.Format("{0}, {1}", it.GetRuntimeTargetTexture().height, it.GetRuntimeTargetTexture().height));
        }
    }

    protected void RegisterDisplay()
    {
        if(displayIdDebug > -1 || VLMaster.Instance.currDisplayNetObj == null) return;

        NetworkObject = VLMaster.Instance.currDisplayNetObj;
        displayIdDebug = DisplayID = VLMaster.Instance.currDisplayInfo.index;
        Info = VLMaster.Instance.currDisplayInfo;

        VLMaster.Instance.SetDisplay(DisplayID, NetworkObject.localIP, gameObject);

        SocketIO.SocketIOComponent sock = NetworkObject.socketObj.GetComponent<SocketIO.SocketIOComponent>();
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["id"] = DisplayID.ToString();
        sock.Emit("monitorRegistered", new JSONObject(data));
        sock.On("monitorCapture", MonitorUpdate);
        sock.On("open", OnOpen);

        DebugConsole.Log(string.Format("Found display {0} {1}@{2}", gameObject.name, DisplayID, NetworkObject.localIP));
    }

    public void SwitchTarget(string b64Image)
    {
        Debug.Log("Swapping");
        // Create new Texture2D for the new image
        byte[] img = System.Convert.FromBase64String(b64Image);
        Texture2D newTex = nt = new Texture2D(1,1);
        newTex.LoadImage(img);

        Debug.Log("Tex Made");
        // Pass our image to the RuntimeImageSource    
        ObjectTracker ot = TrackerManager.Instance.GetTracker<ObjectTracker>();
        ot.RuntimeImageSource.SetImage(newTex, Info.physicalWidth, mTrackableBehaviour.Trackable.Name);
        Debug.Log("Image Set");
        // Remove the old Trackable and create a new one using this object
        Trackable t = mTrackableBehaviour.Trackable;
        if(ourDataSet == null)
            ourDataSet = ot.CreateDataSet();
        else
        {
            try
            {
                ot.DeactivateDataSet(ourDataSet);
                ourDataSet.Destroy(t, false);
            }
            catch
            {
                ourDataSet = ot.CreateDataSet();
            }
        }
        TrackerManager.Instance.GetStateManager().DestroyTrackableBehavioursForTrackable(t,false);
        mTrackableBehaviour = ourDataSet.CreateTrackable(ot.RuntimeImageSource, gameObject);
        mTrackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStatusChanged);
        ot.ActivateDataSet(ourDataSet);
        Debug.Log("Done?");
    }

    public void MonitorUpdate(SocketIO.SocketIOEvent e)
    {
        JSONObject data = e.data;
        if((int)data["id"].n != DisplayID) return;

        // Don't bother switching if we're being tracked, this will actually make us lose tracking!
        if(mTrackableBehaviour.CurrentStatus == TrackableBehaviour.Status.TRACKED) return;

        // Cut off the metadata
        string str = data["img"].str;
        str = str.Split(',')[1];

        SwitchTarget(str);
    }

    public void OnOpen(SocketIO.SocketIOEvent e)
    {
        // Let the app know we're here in case we had to reconnect
        SocketIO.SocketIOComponent sock = NetworkObject.socketObj.GetComponent<SocketIO.SocketIOComponent>();
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["id"] = DisplayID.ToString();
        sock.Emit("monitorRegistered", new JSONObject(data));
    }
}

