﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Pipe : MonoBehaviour {

	[HideInInspector]
	public Transform pointA, pointB;
	public int lineSteps = 20;
	public float Amplitude = 0.3f;
	public Vector3 normal = Vector3.up;
	LineRenderer lr;
	Vector3 lastA, lastB;

	void Awake()
	{
		if(pointA != null && pointB != null)
			MakeLineConnection(pointA.position, pointB.position);
	}

	void Start()
	{
		lr = GetComponent<LineRenderer>();
	}

	void Update()
	{
		//TODO: Only have to call this when a target transform updates
		if(pointA != null && pointB != null)
			MakeLineConnection(pointA.position, pointB.position);
		else // Kill the connection if one of the GameObjects is destroyed
			Destroy(gameObject);
	}

	void MakeLineConnection(Vector3 a, Vector3 b)
	{
		//if (lastA != null && lastB != null && lastA == a && lastB == b)
		//	return;

		lr.positionCount = lineSteps + 1;

		// Calculate position of control point
		Vector3 midpoint = a + (b - a) / 2;
		Vector3 axis = Vector3.Cross(b - a, Vector3.forward);
		Vector3 control = Quaternion.AngleAxis(60f, axis) * (midpoint - a) + a;

		Vector3[] controlPoints = new[] { a, control, b };
		//Vector3[] points = new Vector3[lineSteps + 1];
		Vector3[] points = new Vector3[lineSteps + 1];
		for (int i = 0; i < lineSteps + 1; i++)
		{
			//points[i] = Vector3.Lerp(a,b,(float)i/(float)lineSteps) + 
			//normal * Amplitude * Mathf.Sin((float)i/(float)lineSteps * 180.0f * Mathf.Deg2Rad);
			points[i] = GetBezierPoint(controlPoints, (float)i / (float)lineSteps);
		}
		lr.SetPositions(points);

		lastA = a;
		lastB = b;
	}

	// Derived from https://stackoverflow.com/questions/785097/how-do-i-implement-a-b%c3%a9zier-curve-in-c/21642962#21642962
	Vector3 GetBezierPoint(Vector3[] points, float t)
    {
		/*
		Vector3[] points_t = new Vector3[points.Length];
		points.CopyTo(points_t, 0);

		int i = points_t.Length - 1;
		while (i > 0)
        {
			for(int k = 0; k < i; k++)
				points_t[k] = points_t[k] + t * (points_t[k + 1] - points_t[k]);

			i--;
        }

		return points_t[0];
		*/

		return Mathf.Pow(1 - t, 2) * points[0] + 2 * (1 - t) * t * points[1] + t * t * points[2];
    }

	public void SetTargets(Transform a, Transform b)
	{
		pointA = a;
		pointB = b;
	}

	public bool CompareTargets(Transform a, Transform b)
	{
		if ((pointA == a || pointA == b) && (pointB == a || pointB == b))
		{
			return true;
		}else
		{
			return false;
		}
	}
}
