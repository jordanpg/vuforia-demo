﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public struct Connection
{
	public Transform a;
	public Transform b;
}

public class PipeSystem : MonoBehaviour {

	public Pipe pipePrefab;

	private List<Pipe> pipes;

	public Connection[] InspectorConnections;

	void Start () {
		pipes = new List<Pipe>();
	}

	void Update(){
		foreach(Connection c in InspectorConnections)
		{
			if(c.a != null && c.b != null) Connect(c.a, c.b);
		}
	}

	public bool Connect(Transform a, Transform b)
	{
		foreach(Pipe p in pipes)
		{
			// Check for deleted pipes
			if(p == null)
			{
				pipes.Remove(p);
				continue;
			}

			if(p.CompareTargets(a, b))
			{
				return false;
			}
		}
		Pipe connection = 
			GameObject.Instantiate(pipePrefab, transform.position, Quaternion.identity, transform).GetComponent<Pipe>();
		connection.SetTargets(a,b);
		pipes.Add(connection);
		return true;
	}

	public void ConnectRandom()
	{
		GameObject[] selections = GameObject.FindGameObjectsWithTag("selection");

		int a = 0;
		int b = 0;
		while (a == b)
		{
			a = Random.Range(0,selections.Length-1);
			b = Random.Range(0,selections.Length-1);
		}
		Connect(selections[a].transform, selections[b].transform);
	}
}