﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Selection
{
	[Range(0,1)]
	[Tooltip("minimum x value of the bounding box. From 0 to 1, with 0 being the lefthand side.")]
	public float x_min;
	[Range(0,1)]
	[Tooltip("maximum x value of the bounding box. From 0 to 1, with 1 being the righthand side.")]
	public float x_max;
	[Range(0,1)]
	[Tooltip("minimum y value of the bounding box. From 0 to 1, with 0 being the bottom.")]
	public float y_min;
	[Range(0,1)]
	[Tooltip("minimum y value of the bounding box. From 0 to 1, with 1 being the top.")]
	public float y_max;
	[HideInInspector]
	public GameObject obj;
}
[ExecuteInEditMode]
public class MonitorSelection : MonoBehaviour {

[Header("Display Parameters")]
	[Tooltip("length along diagonal in inches.")]
	public float size;
	[Tooltip("aspect ratio. Common are 4:3, 16:9, etc")]
	public Vector2 aspect;
	[Tooltip("How big is the bezel on horizontal edges of the display?")]
	public float bezelWidth = 0.1f;
	[Tooltip("How big is the bezel on vertical edges of the display?")]
	public float bezelHeight = 0.1f;
	[Tooltip("How far off the screen should the selections be drawn?")]
	public float selectionHeightOffScreen = 0.01f;
	[Tooltip("What is the minimum size that a selection can be in the X or Y component?")]
	public float selectionThresholdSize;
	[Tooltip("This is an array of all current selections defined on this display.")]
	public Selection[] selections;
[Header("Housekeeping")]
	public Material bezelMat;
	public GameObject selectionPrefab;
	public GameObject Empty;
	public float scaleFactor = 10.0f;

	Vector2 realWorldSize;
	Vector3 screenBounds_bottom_left;
	Vector3 screenBounds_top_right;
	Vector3 selection_center;
	GameObject selection;
	GameObject display;
	GameObject bl;
	GameObject tr;
	int frameCount = 0;
	Vector3 startPos;
	Quaternion startRot;
	public Transform tracker;
	Transform oldParent;
	[HideInInspector]
	public bool tracked = false;
	
	const float inchesToMeters = 0.0254f;

	void OnDrawGizmos()
	{
		if(bl != null) Gizmos.DrawSphere(bl.transform.position, 0.1f);
		if(tr != null) Gizmos.DrawSphere(tr.transform.position, 0.1f);
	}

	void Awake()
	{
		transform.localScale = new Vector3(1,1,1);
		startPos = transform.position;
		startRot = transform.rotation;
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		Initialize();
	}

	public void Initialize()
	{
		tracked = true;
		display = GameObject.CreatePrimitive(PrimitiveType.Cube);
		display.transform.SetParent(transform); 
		display.transform.localPosition = Vector3.zero;
		//Calculate the size of the monitor in meters
		//pythagorean theorem + aspect ratio
		//size = sqrt((aspect.x * a)^2 + (aspect.y * a)^2))
		//solve for a
		// size^2 = (aspect.x * a)^2 + (aspect.y * a)^2
		// size^2 = aspect.x^2 * a^2 + aspect.y^2 * a^2
		// size^2 = a^2(aspect.x^2 + aspect.y^2)
		float sizeConstant = Mathf.Sqrt((size * size) / ((aspect.x * aspect.x) + (aspect.y * aspect.y)));
		realWorldSize = aspect * sizeConstant * inchesToMeters * scaleFactor;
		display.transform.localScale = new Vector3(realWorldSize.x, realWorldSize.y, 0.01f);

		GameObject bezel = GameObject.CreatePrimitive(PrimitiveType.Cube);
		bezel.transform.localScale = new Vector3(realWorldSize.x + bezelWidth * 2, realWorldSize.y + bezelHeight * 2, 0.1f);
		bezel.transform.position = transform.position + transform.forward * 0.06f;
		bezel.transform.SetParent(transform);
		bezel.transform.localRotation = Quaternion.Euler(Vector3.zero);
		bezel.GetComponent<MeshRenderer>().sharedMaterial = bezelMat;

		screenBounds_bottom_left = transform.TransformPoint(display.GetComponent<BoxCollider>().bounds.min);
		screenBounds_top_right = transform.TransformPoint(display.GetComponent<BoxCollider>().bounds.max);
		bl = Instantiate(Empty,screenBounds_bottom_left,Quaternion.identity, transform);
		tr = Instantiate(Empty,screenBounds_top_right,Quaternion.identity, transform);
		display.transform.localRotation = Quaternion.Euler(Vector3.zero);
		Vector3 worldTrackerMin = (tracker != null) ? tracker.TransformPoint(tracker.GetComponent<MeshFilter>().mesh.bounds.min) : transform.localPosition;
		Vector3 worldTrackerMax = (tracker != null) ? tracker.TransformPoint(tracker.GetComponent<MeshFilter>().mesh.bounds.max) : transform.localPosition;
		float worldTrackerWidth = worldTrackerMax.x - worldTrackerMin.x;
		float worldTrackerHeight = worldTrackerMax.y - worldTrackerMin.y;
		transform.position = (startPos - display.GetComponent<BoxCollider>().bounds.min 
									   - new Vector3(0,display.GetComponent<BoxCollider>().bounds.size.y,0)) 
									   + new Vector3(-worldTrackerWidth/2,worldTrackerHeight/2,0);
		//transform.rotation = startRot * Quaternion.AngleAxis(90, Vector3.right);
		//gameObject.SetActive(false);
	}


	// void Start () {
	// 	display = GameObject.CreatePrimitive(PrimitiveType.Cube);
	// 	display.transform.SetParent(transform); 
	// 	display.transform.localPosition = Vector3.zero;
	// 	//Calculate the size of the monitor in meters
	// 	//pythagorean theorem + aspect ratio
	// 	//size = sqrt((aspect.x * a)^2 + (aspect.y * a)^2))
	// 	//solve for a
	// 	// size^2 = (aspect.x * a)^2 + (aspect.y * a)^2
	// 	// size^2 = aspect.x^2 * a^2 + aspect.y^2 * a^2
	// 	// size^2 = a^2(aspect.x^2 + aspect.y^2)
	// 	float sizeConstant = Mathf.Sqrt((size * size) / ((aspect.x * aspect.x) + (aspect.y * aspect.y)));
	// 	realWorldSize = aspect * sizeConstant * inchesToMeters * scaleFactor;
	// 	display.transform.localScale = new Vector3(realWorldSize.x, realWorldSize.y, 0.01f);

	// 	GameObject bezel = GameObject.CreatePrimitive(PrimitiveType.Cube);
	// 	bezel.transform.localScale = new Vector3(realWorldSize.x + bezelWidth * 2, realWorldSize.y + bezelHeight * 2, 0.1f);
	// 	bezel.transform.position = transform.position + transform.forward * 0.06f;
	// 	bezel.transform.SetParent(transform);
	// 	bezel.transform.localRotation = Quaternion.Euler(Vector3.zero);
	// 	bezel.GetComponent<MeshRenderer>().sharedMaterial = bezelMat;

	// 	screenBounds_bottom_left = transform.TransformPoint(display.GetComponent<BoxCollider>().bounds.min);
	// 	screenBounds_top_right = transform.TransformPoint(display.GetComponent<BoxCollider>().bounds.max);
	// 	bl = Instantiate(Empty,screenBounds_bottom_left,Quaternion.identity, transform);
	// 	tr = Instantiate(Empty,screenBounds_top_right,Quaternion.identity, transform);
	// 	display.transform.localRotation = Quaternion.Euler(Vector3.zero);
	// 	Vector3 worldTrackerMin = tracker.TransformPoint(tracker.GetComponent<MeshFilter>().mesh.bounds.min);
	// 	Vector3 worldTrackerMax = tracker.TransformPoint(tracker.GetComponent<MeshFilter>().mesh.bounds.max);
	// 	Debug.Log(worldTrackerMax);
	// 	Debug.Log(worldTrackerMin);
	// 	float worldTrackerWidth = worldTrackerMax.x - worldTrackerMin.x;
	// 	float worldTrackerHeight = worldTrackerMax.y - worldTrackerMin.y;
	// 	transform.position = (startPos - display.GetComponent<BoxCollider>().bounds.min 
	// 								   - new Vector3(0,display.GetComponent<BoxCollider>().bounds.size.y,0)) 
	// 								   + new Vector3(-worldTrackerWidth/2,worldTrackerHeight/2,0);
	// 	transform.rotation = startRot * Quaternion.AngleAxis(90, Vector3.right);
	// 	transform.SetParent(tracker);
	// }

	void Update()
	{
		if(!tracked)
		{
			return;
		}
		float selection_width;
		float selection_height;
		Vector3 selection_bottom_left_screen;
		Vector3 selection_top_right_screen;
		for(int i = 0; i < selections.Length; i++)
		{
			if(selections[i].x_min >= selections[i].x_max)
			{
				selections[i].x_min = selections[i].x_max - selectionThresholdSize;
			}
			if(selections[i].y_min >= selections[i].y_max)
			{
				selections[i].y_min = selections[i].x_max - selectionThresholdSize;
			}

			selection_bottom_left_screen = new Vector3(Mathf.Lerp(	bl.transform.localPosition.x, 
																tr.transform.localPosition.x, 
																selections[i].x_min),
													Mathf.Lerp(	bl.transform.localPosition.y,
																tr.transform.localPosition.y,
																selections[i].y_min),
													0);
			selection_top_right_screen = new Vector3(Mathf.Lerp(	bl.transform.localPosition.x,
																	tr.transform.localPosition.x,
																	selections[i].x_max),
														Mathf.Lerp( bl.transform.localPosition.y,
																	tr.transform.localPosition.y,
																	selections[i].y_max),
														0);
			selection_center = new Vector3(	(selection_bottom_left_screen.x + selection_top_right_screen.x)/2.0f,
											(selection_bottom_left_screen.y + selection_top_right_screen.y)/2.0f,
											 -selectionHeightOffScreen);

			selection_width = selection_top_right_screen.x - selection_bottom_left_screen.x;
			selection_height = selection_top_right_screen.y - selection_bottom_left_screen.y;
			if(selections[i].obj == null) {
				selections[i].obj = 
				GameObject.Instantiate(selectionPrefab, transform.position, Quaternion.identity, transform);
			}
			selections[i].obj.transform.localScale = new Vector3(selection_width, selection_height, 1);
			selections[i].obj.transform.localPosition = selection_center;
			selections[i].obj.transform.rotation = transform.rotation;
			selections[i].obj.tag = "selection";
		}
	}
}
