﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TrackingDisplay : MonoBehaviour, ITrackableEventHandler {
    
    private TrackableBehaviour mTrackableBehaviour;
 	public MonitorSelection display;
 	GameObject gd;
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }
     
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            // Play audio when target is found
            //display.Initialize();
            gd = GameObject.Instantiate(display.gameObject);
            gd.transform.position = display.transform.position;
            gd.transform.SetParent(transform);
            display.gameObject.SetActive(false);            
        }else
        {
        	if(gd != null)
        	{
        		DestroyImmediate(gd);
        	}
        	display.tracked = false;
        }
    }   
}
